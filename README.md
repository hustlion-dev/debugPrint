# debugPrint

#### 项目介绍

一款在 Lua 中实现的显示文件名与行号、多等级、多输出打印、支持循环表的打印器模块。

#### 软件架构

见博客文章：https://blog.csdn.net/techfield/article/details/82768987


#### 安装教程

* `require("debugPrint")` and you can use `print`.
* set `PRINT_SOURCE` to `true` to print position info

#### 使用说明

```lua
require("debugPrint")

print("hello")

print("hello", "world")
PRINT_SOURCE = false
print("a simple table", {10, 20})

testTable = {}
testTable[#testTable + 1] = {1, 2, 3}
testTable[#testTable + 1] = testTable
testTable[#testTable + 1] = function () oldprint("hello") end
testTable[#testTable + 1] = true
print("a complicated table", testTable)

```

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request