require("debugPrint")

print("hello")

print("hello", "world")
PRINT_SOURCE = false
print("a simple table", {10, 20})

VERBOSITY_LEVEL = VERBOSITY.INFO
Print(VERBOSITY.INFO, 'This is a info level message: ', "hello")
Print(VERBOSITY.DEBUG, 'This is a debug level message: ', "hello")
VERBOSITY_LEVEL = VERBOSITY.DEBUG
Print(VERBOSITY.INFO, 'This is a info level message: ', "hello")
Print(VERBOSITY.DEBUG, 'This is a debug level message: ', "hello")

testTable = {}
testTable[#testTable + 1] = {1, 2, 3}
testTable[#testTable + 1] = testTable
testTable[#testTable + 1] = function () oldprint("hello") end
testTable[#testTable + 1] = true
print("a complicated table", testTable)